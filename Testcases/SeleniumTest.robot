*** Settings ***
Resource             ../Keywords/SeleniumKeywords.robot
Suite Teardown       Close All Browsers
Documentation        Keyword documentation: https://robotframework.org/SeleniumLibrary/SeleniumLibrary.html
...
...                  Kijk goed of er keywords zijn die je kan gebruiken in de Keywords/SeleniumKeywords.robot file

*** Variable ***
${SELENIUM_DELAY}    0
${TEST_URL}          http://cursusclient-kza-student0${GROUP_NR}.online.hotcontainers.nl/cursussen
${GROUP_NR}          2

${CURSUS_NAAM}       KzaStudent02

*** Test Cases ***
Opdracht 1: Login in de cursusclient
    Open Browser In Jenkins   ${TEST_URL}
    Click Element   id=login    modifier=False
    Wait Until Page Contains Element    id=logout       timeout=2

Opdracht 2: Controleer of de pagina correct geladen is
    Wait Until Page Contains    Cursus kalender     timeout=2

Opdracht 3: Open het scherm om een nieuwe training aan te maken
    Click Element   id=create
    Wait Until Page contains    Cursus aanmaken     timeout= 2

Opdracht 4: Maak een nieuwe training aan
    Input Text  id=trainingName     text=${CURSUS_NAAM}
    Select From List By Index   id=selectAttitude     0
    Select From List By Index   id=functieniveau     0
    Select From List By Index  id=passingCriteria     0
    Select From List By Index   id=status     0
    Input Text  id=maxParticipants     text='5'

    Input Text  id=trainerName     text='Hans'
    Input Text  id=trainerEmail     text='Hans@avans.nl'
    #Click Element       id=addTeacher
    #Wait Until Page contains    id=trainerName      text='Hans'

    Click Element       id=setDefaultDate

    Input Text  id=trainingDescription     text='Mooie beschrijving'
    #Scroll Element Into View    id=save
    sleep   10
    Click Element   id=save

    Wait Until Page contains    Cursus details      timeout=2


Opdracht 5: Meld je aan voor de training die je net hebt gemaakt

    Click Element   id=aanmelden
    sleep       0.5
    Wait Until Page contains    Weet je het zeker?      timeout=2
    Click Element   id=ja

Opdracht 6: Controleer of de aangemaakte training in de lijst van trainingen terugkomt
    Click Element    id = terug
    Wait Until Page contains       ${CURSUS_NAAM}

Opdracht 7: Controleer de details pagina van de aangemaakte training
    Select Cursus From Cursuslist       ${CURSUS_NAAM}
    Click Cursus Details Button
    Wait Until Page contains        Cursus details      timeout=1
    Click Element   id=terug

Opdracht 8: Verwijder de aangemaakte training
    Select Cursus From Cursuslist       ${CURSUS_NAAM}
    Click Cursus Verwijderen Button
    sleep       0.5
    Click Element   id=yesImSure
    Wait Until Page Does Not contain        ${CURSUS_NAAM}      timeout=5

Opdracht 9: Test de logout functionaliteit
    Click Element   id=logout
    Wait Until Page contains        Welkom bij KZA Connected    timeout=1
